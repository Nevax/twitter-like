class CreateHashtags < ActiveRecord::Migration[5.2]
  def change
    create_table :hash_tags do |t|
      t.string :name
      t.timestamps
    end
    add_index :hash_tags, :name, unique: true, null: false
  end
end
