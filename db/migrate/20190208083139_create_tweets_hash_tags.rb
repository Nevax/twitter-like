class CreateTweetsHashTags < ActiveRecord::Migration[5.2]
  def change
    create_table :tweets_hash_tags, id: false do |t|
      t.belongs_to :tweet, index: true
      t.belongs_to :hash_tag, index: true
    end
  end
end
