require 'test_helper'

class FollowTest < ActiveSupport::TestCase

  def setup
    @john_doe = User.create(
        name: 'John Doe',
        email: 'JOHn@example.com',
        password: 'secretsecret', password_confirmation: 'secretsecret')
    @toto = User.create(
        name: 'toto',
        email: 'toto@example.com',
        password: 'secretsecret', password_confirmation: 'secretsecret')
    @follow = Follow.new(follower_id: @john_doe.id, followed_id: @toto.id)
  end

  test "should be valid" do
    assert @follow.valid?
  end

  test "should require a follower_id" do
    @follow.follower_id = nil
    assert_not @follow.valid?
  end

  test "should require a followed_id" do
    @follow.followed_id = nil
    assert_not @follow.valid?
  end

  test "should follow and unfollow a user" do
    assert_not @john_doe.following?(@toto)
    @john_doe.follow(@toto)
    assert @john_doe.following?(@toto)
    assert @toto.followers.include?(@john_doe)
    @john_doe.unfollow(@toto)
    assert_not @john_doe.following?(@toto)
  end
end
