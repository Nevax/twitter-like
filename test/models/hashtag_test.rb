require 'test_helper'

class HashTagTest < ActiveSupport::TestCase


  test "hashtag should be valid" do
    hastag = HashTag.new(name: 'hashtag')

    assert hastag.valid?
  end

  test "hashtag name cannot be empty" do
    hastag = HashTag.new(name: '')

    assert_not hastag.valid?
  end


  test "hashtag name cannot be too long" do
    hastag = HashTag.new(name: 'hashtag' * 20)

    assert_not hastag.valid?
  end

  test "an hash tag is related to on tweet" do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet'
    tweet = user.tweets.create(body: body)

    assert_same 0, tweet.hash_tags.count
    tweet.hash_tags.create(name: 'hashtag')

    assert_same 1, tweet.hash_tags.count
  end

  test "an hashtag is created on user tweet" do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet #test'
    tweet = user.tweets.create(body: body)

    assert_same 1, tweet.hash_tags.count
    assert_equal "#test", tweet.hash_tags.last.name
  end

  test "an hashtag is created on user tweet update" do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet #test'
    tweet = user.tweets.create(body: body)

    assert_same 1, tweet.hash_tags.count
    assert_equal "#test", tweet.hash_tags.last.name
    tweet.body = 'Random tweet #test #second'
    tweet.save
    tweet.reload

    assert_same 2, tweet.hash_tags.count
    assert_equal "#test", tweet.hash_tags.first.name
    assert_equal "#second", tweet.hash_tags.last.name
  end

  test "an hashtag is un-associated on user tweet update" do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet #test'
    tweet = user.tweets.create(body: body)

    assert_same 1, tweet.hash_tags.count
    assert_equal "#test", tweet.hash_tags.last.name
    tweet.body = 'Random tweet without hashtag'
    tweet.save
    tweet.reload

    assert_same 0, tweet.hash_tags.count
  end

  test "an hash tag can be in relation with multiple tweets" do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet #test'
    tweet = user.tweets.create(body: body)
    assert_same 1, tweet.hash_tags.count

    new_tweet = user.tweets.create(body: body)
    assert_same 1, new_tweet.hash_tags.count
  end
end
