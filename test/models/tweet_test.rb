require 'test_helper'

class TweetTest < ActiveSupport::TestCase
  test 'user post a tweet' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'Random tweet'

    user.tweets.create(body: body, user_id: user.id)

    assert_equal 1 , user.tweets.count
    assert_equal body, user.tweets.last.body
  end

  test 'user post a tweet with an empty body' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )

    user.tweets.create(body: '', user_id: user.id)

    assert_equal 0 , user.tweets.count
  end

  test 'user cannot post a tweet with too long body' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'A' * 143

    user.tweets.create(body: body, user_id: user.id)

    assert_equal 0 , user.tweets.count
    assert_not_nil user.tweets.last.errors[:body][0]
  end

  test 'user cannot post a tweet with empty body' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = ''

    user.tweets.create(body: body, user_id: user.id)

    assert_equal 0 , user.tweets.count
    assert_not_nil user.tweets.last.errors[:body][0]
  end

  test 'user can post a tweet with 142 long body' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'A' * 142

    user.tweets.create(body: body, user_id: user.id)

    assert_equal 1 , user.tweets.count
    assert_nil user.tweets.last.errors[:body][0]
  end


  test 'user can not post a too short tweet' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )
    body = 'a'

    user.tweets.create(body: body, user_id: user.id)

    assert_equal 0 , user.tweets.count
    assert user.tweets.last.errors[:body][0]
  end
end
