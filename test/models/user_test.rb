require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'valid user' do
    user = User.new(name: 'John', email: 'john@example.com', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')
    assert user.valid?
  end

  test 'invalid without name' do
    user = User.new(email: 'john@example.com')
    refute user.valid?, 'user is valid without a name'
    assert_not_nil user.errors[:name], 'no validation error for name present'
  end

  test 'cannot create without email' do
    user = User.new(name: 'John', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')

    refute user.valid?

    assert_not_nil user.errors[:email][0]
  end

  test 'cannot create two users with the same email' do
    email = 'john@example.com'
    User.create(name: 'John', email: email, password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')

    duplicate_user = User.new(name: 'John Doe', email: email)

    assert_not duplicate_user.valid?
    assert_not_nil duplicate_user.errors[:email][0]
  end

  test 'cannot create two users with the same name' do
    name = 'John Doe'
    User.create(name: name, email: 'john@example.com', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')

    duplicate_user = User.new(name: name, email: 'john@example.com')

    assert_not duplicate_user.valid?
    assert_not_nil duplicate_user.errors[:name][0]
  end

  test 'cannot create an user with too short name' do
    user = User.create(name: 'Jo', email: 'john@example.com', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')

    assert_not user.valid?
    assert_not_nil user.errors[:name][0]
  end

  test 'cannot create an user with too long name' do
    user = User.create(
        name: 'JoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJoJohn',
        email: 'john@example.com',
        password: 'j0nhD0€Pass',
        password_confirmation: 'j0nhD0€Pass'
    )

    assert_not user.valid?
    assert_not_nil user.errors[:name][0]
  end

  test 'cannot create an user with malformed email' do
    user = User.create(name: 'Jo', email: 'thisIsNotAnEmail', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass')

    assert_not user.valid?
    assert_not_nil user.errors[:email][0]
  end

  test 'cannot create with too long email' do
    user = User.create(
        name: 'John Doe',
        email: 'johnjohnjohnjohnjohnjohnjohnjohnjohnjohnjohnjohnjohnjohn@example.com',
        password: 'j0nhD0€Pass',
        password_confirmation: 'j0nhD0€Pass'
    )

    assert_not user.valid?
    assert_not_nil user.errors[:email][0]
  end

  test 'cannot create with too short password' do
    user = User.create(name: 'John Doe', email: 'john@example.com', password: 'secret', password_confirmation: 'secret')

    assert_not user.valid?
    assert_not_nil user.errors[:password][0]
  end

  test 'cannot create with too long password' do
    password = 'secret' * 20
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password,
        password_confirmation: password
    )

    assert_not user.valid?
    assert_not_nil user.errors[:password][0]
  end

  test 'cannot create with empty password' do
    password = ''
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password,
        password_confirmation: password
    )

    assert_not user.valid?
    assert_not_nil user.errors[:password][0]
  end

  test 'password is hashed' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'john@example.com',
        password: password, password_confirmation: password
    )

    assert user.valid?
    assert_not_same password, user.password_digest
  end

  test 'email is saved in lowercase' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'JOHn@example.com',
        password: password, password_confirmation: password
    )

    assert_equal 'john@example.com', user.email
  end

  test 'passwords are not the same' do
    password = 'secret' * 2
    user = User.create(
        name: 'John Doe',
        email: 'JOHn@example.com',
        password: password, password_confirmation: password + "toto"
    )

    assert_not user.valid?
    assert_not_nil user.errors[:password_confirmation][0]
  end

  test 'valid with picture url' do
    user = User.new(name: 'John', email: 'john@example.com', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass', picture: 'https://via.placeholder.com/350x150')
    assert user.valid?
  end

  test 'valid with malformed picture url' do
    user = User.new(name: 'John', email: 'john@example.com', password: 'j0nhD0€Pass', password_confirmation: 'j0nhD0€Pass', picture: 'https:/via.placeholder.com/350x150')
    assert_not user.valid?
  end

end
