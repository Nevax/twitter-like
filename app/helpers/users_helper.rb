module UsersHelper
  def button_text
    if controller.action_name == "new" || controller.action_name == "create"
      return "Create User"
    elsif controller.action_name == "edit" || controller.action_name == "update"
      return "Update User"
    else
      return controller.action_name
    end
  end
end
