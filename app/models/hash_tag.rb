class HashTag < ApplicationRecord

  validates :name,
            uniqueness: true,
            presence: true,
            length: {minimum: 2, maximum: 42}

  has_and_belongs_to_many :tweets
end
