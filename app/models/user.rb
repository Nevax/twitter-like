class User < ApplicationRecord

  has_many :follows, dependent: :destroy
  has_many :active_relationships, class_name: "Follow",
           foreign_key: "follower_id",
           dependent: :destroy
  has_many :passive_relationships, class_name:  "Follow",
           foreign_key: "followed_id",
           dependent:   :destroy
  has_many :followers, through: :passive_relationships, source: :follower

  has_many :tweets, dependent: :destroy
  has_many :following, through: :active_relationships, source: :followed

  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  has_secure_password
  before_save {email.downcase!}
  validates :name,
            presence: true,
            uniqueness: true,
            length: {minimum: 3, maximum: 50}

  validates :email,
            presence: true,
            uniqueness: true,
            length: {minimum: 3, maximum: 50},
            format: {with: EMAIL_REGEX}

  validates :picture,
            allow_nil: true,
            url: true,
            length: {maximum: 255}

  validates :password,
            presence: true,
            confirmation: true,
            length: {minimum: 8, maximum: 255}

  # Follows a user.
  def follow(other_user)
    following << other_user
  end

  # Unfollows a user.
  def unfollow(other_user)
    following.delete(other_user)
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end
end
