class Tweet < ApplicationRecord
  belongs_to :user

  validates :body,
            presence: true,
            length: {minimum: 2, maximum: 142}

  has_and_belongs_to_many :hash_tags

  after_save :parse_hash_tags

  def parse_hash_tags
    hash_tags_array = []
    self.body.scan(/#\w+/).flatten.each do |hash_tag|
      hash_tags_array.push(HashTag.where(name: hash_tag).first_or_create)
    end
    self.hash_tags = hash_tags_array
  end
end
