class FollowsController < ApplicationController

  # POST /follows
  def create
    @user = User.find(params[:user_id])
    @following_user = User.find(params[:following_user_id])

    respond_to do |format|
      if @following_user.follow(@user)
        format.html {redirect_to user_path(@user), notice: 'Successfully followed.'}
      else
        format.html {redirect_to user_path(@user), error: 'Something went wrong.'}
      end
    end
  end

  # DELETE /follows/1
  def destroy
    @user = User.find(params[:user_id])
    @following_user = User.find(params[:following_user_id])

    respond_to do |format|
      if @following_user.unfollow(@user)
        format.html {redirect_to user_path(@user), notice: 'Successfully unfollowed.'}
      else
        format.html {redirect_to user_path(@user), error: 'Something went wrong.'}
      end
    end
  end

end
