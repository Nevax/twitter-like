class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  def index
    @users = User.all
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end

  # GET /users/1
  def show
    @tweets = Tweet.all
    @users = User.all
    @followers_tweets = Array.new
    @hash_tags = HashTag.all
    @user.followers.each do |follow|
      follow.tweets.each do |new_tweets|
        @followers_tweets.push(new_tweets)
      end
    end

  end

  # GET /users/new
  def new
    @user = User.new
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end

  # GET /users/1/edit
  def edit
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end

  # POST /users
  def create
    @user = User.new(user_params)
    @tweets = Tweet.all
    @hash_tags = HashTag.all
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /users/1
  def update
    @tweets = Tweet.all
    @hash_tags = HashTag.all
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /users/1
  def destroy
    @tweets = Tweet.all
    @hash_tags = HashTag.all
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :email, :password, :picture)
    end
end
