class WelcomeController < ApplicationController
  def index
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end
end
