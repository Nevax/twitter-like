class TweetsController < ApplicationController
  before_action :set_tweet, only: [:show, :edit, :update, :destroy]

  # GET /tweets
  def index
    @user_tweets = User.find(params[:user_id]).tweets.order(:created_at)
    @user =  User.find(params[:user_id])
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end

  # GET /tweets/1
  def show
  end

  # GET /tweets/new
  def new
    @tweets = Tweet.all
    @tweet = Tweet.new
    @hash_tags = HashTag.all
  end

  # GET /tweets/1/edit
  def edit
    @tweets = Tweet.all
    @hash_tags = HashTag.all
  end

  # POST /tweets
  def create
    @user = User.find(params[:user_id])
    @tweet = @user.tweets.create(tweet_params)
    @hash_tags = HashTag.all
    respond_to do |format|
      if @tweet.save
        format.html { redirect_to user_tweets_path(@user), notice: 'Tweet was successfully created.' }
      else
        format.html { redirect_to user_tweets_path(@user), error: 'Something went wrong' }
      end
    end
  end

  # PATCH/PUT /tweets/1
  def update
    respond_to do |format|
      if @tweet.update(tweet_params)
        format.html { redirect_to @tweet, notice: 'Tweet was successfully updated.' }
      else
        format.html { redirect_to user_tweets_path(@user), error: 'Something went wrong' }
      end
    end
  end

  # DELETE /tweets/1
  def destroy
    @user = User.find(params[:user_id])
    @tweet = @user.tweets.find(params[:id])
    @tweet.destroy
    respond_to do |format|
      format.html { redirect_to user_tweets_path(@user), notice: 'Tweet was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tweet
      @tweet = Tweet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tweet_params
      params.require(:tweet).permit(:body, :user_id)
    end
end
