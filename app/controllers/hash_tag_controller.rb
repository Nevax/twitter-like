class HashTagController < ApplicationController


  # GET /hash_tag/1
  def show
    @tweets = Tweet.all
    @hash_tags = HashTag.all
    @hash_tag = HashTag.find(params[:id])
  end

end
