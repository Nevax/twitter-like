# Twitter Light

Cette application est une version simplifier de Twitter développé en Ruby avec le framework Ruby on Rails.

## Mise en place

### Prérequis

- Ruby 2.5.3
- Rails 5.2.1
- MySQL

### Mise en place

Configurer la connextion à la base de données `config/database.yml`

Installation des dépendences
```bash
bundle install
```

Création de la base de données et des tables
```bash
bin/rails db:setup
```

Ajout des données de test (optionel)
```bash
bin/rails db:seed
```

Lancement du serveur
```bash
bin/rails server
```

## Organisation

Binôme : Antoine Gravelot / Julien Nantet 

Répartition des tâches :

- Antoine (majoritairement Backend) : 
    - Model (Utilisateurs, Tweets, Hashtag, Follow)
    - Constrollers
    - Base de données
    - Test Unitaires
- Julien (majoritairement Frontend) :
    - Views
    - Style
    - Controller + Vue pour les hashtags

## Fonctionnalités

- Listing des utilisateurs
- Espace pour chaque utilisateurs
- Création / Modification d'un utilisateur
- Création de tweet pour un utilisateur
- Modification de ces tweets pour un utilisateur
- Gestion des hashtags pour chaque tweets
- Possibilité de suivre d'autres utilisateurs (et d'arrêter de suivre)
- Affichage des derniers tweets + hashtags les plus populaires

## Choix d'implémentation

Framework JS + CSS : Bootstrap
Utilisation de celui-ci car Julien l'utilise quotidiennement et donc le maîtrise bien.

Icones : FontAwesome
Utilisation simple est bonne compatilibilité avec Bootstrap

## Dificultés rencontrées

Manque d'apprentisage et de maîtrise de Ruby en lui-même donc difficile au début d'utilisé Rails.
Utilisation de Ruby trés difficile sous windows pour Julien.

## Améliorations possibles

Gestion des photos de profils directement sur le site au lieu d'une url externe.
Mise en place d'un système de connexion pour chaque utiliseur afin d'acceder a son espace dédier.
Injecter la liste des tweets dans la vue application pour éviter les répétitions dans les controllers.
Améliorer l'interface en général.
Utiliser des framework frontend plus récent (bulma, Vue.js) -> Buefy
Utiliser un algorithme de chiffrement plus fort puisque bcrypt est trops simple à forcer, par example argon2

## Vos impression sur Ruby & Rails

Julien : Le framework Rails à l'air bien complet et doit être performant lorsqu'il est bien maîtriser mais le manque de compatibilité avec windows m'a fait perdre trop de temps.

Antoine : Vu que je m'intéresse fortement à Laravel, j'ai pu retrouver rapidement mes marques puisque celui-ci en est inspiré.
          J'apprécie la communauté fortement tournée vers les tests.
          Je trouve la syntaxe agréable, elle est plus légère que celle de PHP.
          Je serais heureux d'avoir la possibilité de le mettre en place dans un vrai projet,
            mais j'ai des doutes sur sa pérennité sur le long terme, puisque sa popularité est en baisse.
            
            