Rails.application.routes.draw do
  resources :hash_tag and
  resources :users do
    resources :tweets
  end
  get 'welcome/index'

  root 'welcome#index'

  resources :follows, only: [:create, :destroy]
end
